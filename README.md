Fubber Framework Installer
==========================

Simplifies setting up a new Fubber Framework application.

Use:

    composer create-project ennerd/fubber-installer
